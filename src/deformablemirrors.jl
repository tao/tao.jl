#
# DeformableMirrors.jl --
#
# API for deformable mirrors in TAO.
#
#-------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2022, Éric Thiébaut.
#

module DeformableMirrors

export
    AbstractDeformableMirror,
    DeformableMirror,
    device,
    layout,
    reference,
    reset,
    set_reference!,
    send,
    send!

using ..Tao
using ..Tao: indexed_layout
import ..Tao:
    reset, send, send!, reference, set_reference!, device, layout,
    floating_point_type

using Base: throw_boundserror, @propagate_inbounds
import Base: getindex, setindex!, checkbounds, flush

using Statistics # FIXME: it is provided by Tao?

"""
    AbstractDeformableMirror{T}

is the abstract super-type of deformable mirror types.  Parameter `T` is the
element type of the actuators commands.

The available methods for a deformable mirror `dm` are:

- `send(dm, cmd, mrk, tm=Inf)` sends the set of commands `cmd` (relative to the
  reference), with a mark `mrk` and an optional timeout `tm`;

- `reset(dm, tm=Inf)` resets the deformable mirror as if null commands have
  been sent;

- `minimum(dm)`, `maximum(dm)`, and `extrema(dm)` yield the minimum, maximum,
  and extreme values of an actuator command;

- `length(dm)` yields the number of actuators;

- `size(dm)` yields the dimensions of the grid of actuators;

- `layout(dm)` yields the layout of the actuators;

- `floating_point_type(dm)` yields the floating-point type of the actuators
  commands.

"""
abstract type AbstractDeformableMirror{T<:AbstractFloat} end

Base.extrema(dm::AbstractDeformableMirror) = (minimum(dm), maximum(dm))

floating_point_type(dm::AbstractDeformableMirror) =
    floating_point_type(typeof(dm))
floating_point_type(::Type{<:AbstractDeformableMirror{T}}) where {T} = T

# Fake deformable mirror for testing.
struct FakeDeformableMirrorDevice{T<:AbstractFloat}
    len::Int
end
FakeDeformableMirrorDevice(::Type{T}, len::Integer) where {T} =
    FakeDeformableMirrorDevice{T}(len)
Base.length(dev::FakeDeformableMirrorDevice) = dev.len
Base.eltype(dev::FakeDeformableMirrorDevice{T}) where {T} = T
function send(dev::FakeDeformableMirrorDevice{T},
              cmd::AbstractVector{T}) where {T}
    @assert !Base.has_offset_axes(cmd)
    @assert length(cmd) == length(dev)
end

"""
    Tao.DeformableMirror([T = eltype(dev),] dev, inds)
    Tao.DeformableMirror([T = eltype(dev),] dev, msk; orient=0)

yields an instance of `Tao.DeformableMirror` using device `dev` for sending
commands and assuming the layout of the actuators is given by calling
[`Tao.indexed_layout`](@ref) with the subsequent arguments and, possibly, with
keyword `orient` which specifies the orientation of the actuators numbering.

The encapsulation by `Tao.DeformableMirror` is to implement a common interface
for different kinds of deformable mirrors.  This interface accounts for a set
of reference commands and provides more information such as the mirror
layout.

The available methods for a deformable mirror `dm` are:

```julia
set_reference!(dm, ref) # to set the reference commands
reference(dm)           # to get the reference commands
send(dm, cmd)           # to buffer a set of commands relative to the
                        # reference ones and send them to the device
reset(dm)               # to apply the reference commands
layout(dm)              # to get the layout of the actuators
flush(dm)               # to send (or re-send) the buffered commands
close(dm)               # to close the deformable mirror device
minimum(dm)             # yields the minimum value of a command
maximum(dm)             # yields the maximum value of a command
extrema(dm)             # yields the minimum and maximum values of a command
```

A deformable mirror instance owns two internal sets of commands: the reference
commands and a buffer of commands.

A deformable mirror instance can be used as an array to query or set the value
of a given buffered command:

```julia
dm[i]        # yields the `i`-th buffered command
dm[i] = val  # sets the value of the `i`-th buffered command
```

Note that `val` and `dm[i]` are both relative to the `i`-th reference command.

!!! note

    The levels given by `minimum(dm)`, `maximum(dm)` and `extrema(dm)` are
    relative to the mean of the reference commands and are thus only
    indicative.  They can only be exact if the reference commands all have the
    same value.  A better approximation of the limits is actuator-wise and
    given by:

    ```julia
    cmin = minimum(device(dm)) .- reference(dm)
    cmax = maximum(device(dm)) .- reference(dm)
    ```

The following methods should be applicable to the device `dev`:

```julia
length(dev)     # to get the number of actuators
eltype(dev)     # to get the type of the individual commands
send(dev, cmd)  # to send a set of commands
send!(dev, cmd) # like `send` but copy effective commands in `cmd`
close(dev)      # to close/release the device
minimum(dev)    # yields the minimum value of a command
maximum(dev)    # yields the maximum value of a command
```

Example:

```julia
using Tao, Alpao, Themis
dm = Tao.DeformableMirror(Alpao.DeformableMirror("BOL143"),
                          Themis.DM_SHAPE, orient=5)
```

"""
struct DeformableMirror{T<:Real,M} <: AbstractDeformableMirror{T}
    dev::M            # device
    len::Int          # number of actuators
    ref::Vector{T}    # reference command
    buf::Vector{T}    # command buffer
    inds::Matrix{Int} # layout (array of indices)
    function DeformableMirror{T,M}(dev::M, len::Int,
                                   inds::Matrix{Int}) where {T<:Real,M}
        return new{T,M}(dev, len, zeros(T, len), zeros(T, len), inds)
    end
end

DeformableMirror(dev::M, inds::AbstractMatrix{<:Integer}) where {M} =
    DeformableMirror(eltype(dev), dev, inds)

DeformableMirror(dev::M, msk::AbstractMatrix{Bool}; kwds...) where {M} =
    DeformableMirror(eltype(dev), dev, msk; kwds...)

function DeformableMirror(::Type{T},
                          dev::M,
                          inds::AbstractMatrix{<:Integer}) where {T<:Real,M}
    len = length(dev)
    isa(len, Integer) && len > 0 || error("invalid number of actuators")
    cnt, xinds = indexed_layout(inds)
    cnt == len || error("invalid number of non-zero indices")
    return DeformableMirror{T,M}(dev, len, xinds)
end

function DeformableMirror(::Type{T},
                          dev::M,
                          msk::AbstractMatrix{Bool};
                          orient::Integer = 0) where {T<:Real,M}
    len = length(dev)
    isa(len, Integer) && len > 0 || error("invalid number of actuators")
    cnt, xinds = indexed_layout(msk, orient)
    cnt == len || error("invalid number of true values in mask")
    return DeformableMirror{T,M}(dev, len, xinds)
end

"""
    Tao.device(dm) -> dev

yields the device associated with the deformable mirror `dm`.

"""
device(dm::DeformableMirror) = dm.dev

"""
    Tao.reference(dm) -> ref

yields the set of reference commands of the deformable mirror `dm`.

"""
reference(dm::DeformableMirror) = dm.ref

"""
    Tao.set_reference!(dm, ref) -> reference(dm)

overwrites the set of reference commands of the deformable mirror `dm` with the
set of commands `ref` and return the resulting reference commands.

"""
set_reference!(dm::DeformableMirror, ref::AbstractVector) =
    copyto!(reference(dm), ref) # FIXME: check length!

buffer(dm::DeformableMirror) = dm.buf
Base.extrema(dm::DeformableMirror) =
    (avg = mean(reference(dm));
     (minimum(device(dm)) - avg, maximum(device(dm)) - avg))
Base.minimum(dm::DeformableMirror) =
    (minimum(device(dm)) - mean(reference(dm)))
Base.maximum(dm::DeformableMirror) =
    (maximum(device(dm)) - mean(reference(dm)))

#FIXME: no needed? Base.eltype(::AbstractDeformableMirror{T}) = T
Base.length(dm::DeformableMirror) = dm.len
Base.size(dm::DeformableMirror) = (length(dm),)
Base.show(io::IO, ::MIME"text/plain", dm::DeformableMirror{T,M}) where {T,M} =
    print(io, "DeformableMirror{$T,$M} with ", length(dm), " actuators\n")
Base.close(dm::DeformableMirror) = close(device(dm))

"""
    Tao.layout(dm) -> inds

yields the indices of the actuators of the deformable mirror `dm`.  The result
`inds` is a 2D array of integers in the range `0:n` with `n` the number of
actuators: `inds[i,j]` is `k ∈ 1:n` if the `k`-th actuator is at position
`(i,j)` and `inds[i,j]` is `0` if there are no actuators at position `(i,j)`.

"""
layout(dm::DeformableMirror) = dm.inds

function send(dm::DeformableMirror,
              cmd::AbstractVector{<:Real})
    setbuffer!(dm, cmd)
    flush(dm)
    nothing
end

function send!(dm::DeformableMirror,
               cmd::AbstractVector{<:Real})
    buf = setbuffer!(dm, cmd)
    flush(dm)
    ref = reference(dm)
    # Note that arguments have already been checked.
    @inbounds @simd for i in eachindex(buf, ref, cmd)
        cmd[i] = buf[i] - ref[i]
    end
    return cmd
end

# Store command plus reference in buffer.
function setbuffer!(dm::DeformableMirror{T},
                    cmd::AbstractVector{<:Real}) where {T}
    buf, ref = buffer(dm), reference(dm)
    @inbounds @simd for i in eachindex(buf, ref, cmd)
        buf[i] = T(cmd[i]) + ref[i]
    end
    return buf
end

# Send buffered commands.
flush(dm::DeformableMirror) =
    send!(device(dm), buffer(dm))

function reset(dm::DeformableMirror)
    buf, ref = buffer(dm), reference(dm)
    @inbounds @simd for i in eachindex(buf, ref)
        buf[i] = ref[i]
    end
    return flush(dm)
end

@inline checkbounds(dm::DeformableMirror, i::Int) =
    1 ≤ i ≤ length(dm) || throw_boundserror(dm, i)

@inline getindex(dm::DeformableMirror, i::Int) = begin
    @boundscheck checkbounds(dm, i)
    @inbounds val = buffer(dm)[i] - reference(dm)[i]
    return val
end

@inline setindex!(dm::DeformableMirror, val, i::Int) = begin
    @boundscheck checkbounds(dm, i)
    @inbounds buffer(dm)[i] = reference(dm)[i] + val
    return dm
end

end # module
