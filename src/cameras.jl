#
# cameras.jl --
#
# API for cameras in TAO.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2022, Éric Thiébaut.
#

Base.eltype(cam::AbstractCamera) = eltype(typeof(cam))
Base.eltype(::Type{<:AbstractCamera{T,N}}) where {T,N} = T

Base.ndims(cam::AbstractCamera) = ndims(typeof(cam))
Base.ndims(::Type{<:AbstractCamera{T,N}}) where {T,N} = N

"""
    mean([T=Float64,], cam::AbstractCamera, nimgs; skip=0, timeout=0.5) -> arr

yields the mean of `nimgs` images acquired from camera `cam`.  Keyword
`timeout` may be used to specified the maximum time to wait for an image in
seconds.  Keyword `skip` is to specify the number of initial images to skip.
Optional argument `T` is to specify the floating-point type of the elements of
the result.

For now, this method computes a simple sample mean of pre-processed images
(that is, not accounting for their respective weights).

"""
mean(cam::AbstractCamera, args...; kwds...) =
    mean(Float64, cam, args...; kwds...)

function mean(::Type{T},
              cam::AbstractCamera{P},
              nimgs::Integer;
              skip::Integer = 0,
              timeout::Real = 0.5) where {T<:AbstractFloat,P}
    @assert nimgs > 1
    start(cam)
    A = zeros(T, size(cam))
    n = 0
    while n < nimgs
        img = timedwait(SingleImage, cam, timeout)
        if skip > 0
            skip -= 1
        else
            n += 1
            add!(Vectorize, A, img)
        end
    end
    return scale!(Vectorize, A, 1/nimgs)
end
