#
# times.jl -
#
# Methods related to time in TAO, a Toolkit for Adaptive Optics.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2023, Éric Thiébaut.
#

import Base.Libc: TimeVal

"""
    Tao.build(::Type{T}, ip, fp) -> t::T

yields a time value of type `T<:AbstractTimeValue` with integer part `ip` and
fractional part `fp`.

"""
build(::Type{TimeVal}, ip::Integer, fp::Integer) = TimeVal(ip, fp)

"""
    Tao.resolution(t) -> n

yields the integer `n` such that the resolution of time `t` is `1/n` second.
Argument `t` can be a time value instance or type.

"""
resolution(x::TimeValue) = resolution(typeof(x))
resolution(::Type{TimeVal}) = 1_000_000

"""
    Tao.time_secs(t::AbstractTimeValue) -> ip::Integer

yields the integer part (in seconds) of time value `t`.

    Tao.time_secs(T::Type{<:AbstractTimeValue}) -> ip_type::Type{<Integer}

yields the type of the integer part of time value type `T`.

"""
time_secs(t::TimeVal) = getfield(t, 1)
time_secs(::Type{T}) where {T<:TimeVal} = fieldtype(T, 1)

"""
    Tao.time_frac(t::AbstractTimeValue) -> fp::Integer

yields the fractional part (in `1/n`-th of seconds) of time value `t` with `n`
the resolution of the time value.

    Tao.time_frac(T::Type{<:AbstractTimeValue}) -> fp_type::Type{<Integer}

yields the type of the integer part of time value type `T`.

    Tao.resolution(t)  # yields the integer `n`
    Tao.build(T,ip,fp) # yields a time value of type `T` with integer part `ip` and fractional part `fp`

"""
time_frac(t::TimeVal) = getfield(t, 2)
time_frac(::Type{T}) where {T<:TimeVal} = fieldtype(T, 2)

"""
    Tao.forever

is the singleton instance of `Tao.Forever` used to indicate an unlimited time
limit.

"""
const forever = Forever()

"""
    Tao.seconds([T=Float64,] x) -> secs::T

converts time `x` into a number of seconds with floating-point type `T`. `x`
needs not be normalized.

"""
seconds(t::TimeLimit) = seconds(Float64, t)
seconds(::Type{T}, t::Real) where {T<:AbstractFloat} = convert(T, t)
seconds(::Type{T}, t::Forever) where {T<:AbstractFloat} = convert(T, Inf)
seconds(::Type{T}, t::TimeValue) where {T<:AbstractFloat} =
    T(time_secs(t)) + T(time_frac(t))/T(resolution(t))

"""
    Tao.normalize(t)

yields time value `t` such that the number of fractions of second is
nonnegative and strictly less than one second.

"""
@inline function normalize(t::T) where {T<:TimeValue}
    ip, fp = time_secs(t), time_frac(t)
    res = oftype(fp, resolution(t))
    ip += oftype(ip, div(fp, res))
    fp = rem(fp, res)
    if fp < 0
        ip -= one(ip)
        fp += res
    end
    return build(T, ip, fp)
end

"""
    Tao.convert_time(T, t)

converts time value `t` to type `T`. If `t` is a real, it is assumed to be a
number of (fractional) seconds.

"""
convert_time(::Type{T}, t::T) where {T<:TimeValue} = t
@inline function convert_time(::Type{T}, t::TimeValue) where {T<:TimeValue}
    t = normalize(t)
    ip = time_secs(t)
    fp = time_frac(t)
    inp_res = oftype(fp, resolution(t))
    out_res = oftype(fp, resolution(T))
    if inp_res > out_res
        # Reduced time resolution. Compute the new value of the number of
        # fractions of second by rounding the division to the nearest integer
        # (the expression requires that fp be nonnegative which is guaranteed
        # by the normalization) and normalize again if needed.
        n = div(inp_res, out_res)
        fp = div(fp + (n >> one(n)), n) # division rounded to nearest integer
        if fp >= out_res
            ip += one(ip)
            fp -= out_res
        end
    elseif out_res > inp_res
        fp *= div(out_res, inp_res)
    end
    return build(T, ip, fp)
end

# Convert number of seconds to a time structure.
convert_time(::Type{T}, secs::Integer) where {T<:TimeValue} = build(T, secs, 0)
convert_time(::Type{T}, x::NTuple{2,Integer}) where {T<:TimeValue} =
    convert_time(T, x...)
convert_time(::Type{T}, ip::Integer, fp::Integer) where {T<:TimeValue} =
    normalize(build(T, ip, fp))
function convert_time(::Type{T}, t::Forever) where {T<:TimeValue}
    res = resolution(T)
    return build(T, typemax(time_secs(T)), res - one(res))
end
function convert_time(::Type{T}, secs::AbstractFloat) where {T<:TimeValue}
    isnan(secs) && Priv.argument_error("invalid number of seconds (NaN)")
    # Compute the number of seconds and of fractions of second, rounding to the
    # nearest number of fractions of second.  Then, normalize the number of
    # seconds and of fractions of second.
    T1 = time_secs(T)
    T2 = time_frac(T)
    res = resolution(T)
    fs = floor(secs)
    if fs > typemax(T1)
        return build(T, typemax(T1), res - one(res))
    elseif fs ≤ typemin(T1)
        return build(T, typemin(T1), zero(T2))
    else
        ip = convert(T1, fs)
        fp = round(T2, (secs - fs)*res) # result is ≥ 0
        res = convert(T2, res)
        if fp >= res
            ip += one(ip)
            fp -= res
        end
        return build(T, ip, fp)
    end
end

# Extend some basic functions and operators (but only for AbstractTimeValue and
# Forever to avoid type-piracy).

# Conversion to floating-point yields the time in seconds.
for type in (:Float16, :Float64, :Float32, :BigFloat)
    @eval Base.$type(t::Union{AbstractTimeValue,Forever}) = seconds($type, t)
end
Base.float(t::Union{AbstractTimeValue,Forever}) = seconds(Float64, t)
Base.convert(::Type{T}, t::Union{AbstractTimeValue,Forever}) where {T<:AbstractFloat} =
    seconds(T, t)

# Conversion to another time representation.
function Base.convert(::Type{T}, t::Union{AbstractTimeValue,
                                          Forever,
                                          Real,
                                          NTuple{2,Integer}}) where {T<:TimeValue}
    return convert_time(T, t)
end

# Unary plus and minus.
Base.:(+)(t::AbstractTimeValue) = t
Base.:(-)(t::T) where {T<:AbstractTimeValue} =
    normalize(build(T, -time_secs(t), -time_frac(t)))

# Addition of times.
Base.:(+)(t::T, secs::Real) where {T<:AbstractTimeValue} =
    t + convert_time(T, secs)
Base.:(+)(a::T, b::T) where {T<:AbstractTimeValue} =
    normalize(build(T, time_secs(a) + time_secs(b), time_frac(a) + time_frac(b)))

# Addition of times.
Base.:(-)(t::T, secs::Real) where {T<:AbstractTimeValue} = t - T(secs)
Base.:(-)(a::T, b::T) where {T<:AbstractTimeValue} =
    normalize(build(T, time_secs(a) - time_secs(b), time_frac(a) - time_frac(b)))
