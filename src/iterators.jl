#
# iterators.jl --
#
# Provide methods to build iterators.
#
#------------------------------------------------------------------------------
#
# This file is part of the TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2019, Éric Thiébaut.
#

module Iterators

export
    MapIterator

"""
    MapIterator(call, iter)

returns an iterator which yields `call(x1)`, then `call(x2)`, etc., where `x1`,
`x2`, etc., are the values given by the iterator `iter`.

If the second argument is a number, say `n`, then it is assumed that
`iter=1:n`.

Example:

    for v in MapIterator(x -> x + 3x^2, 3)
        ....
    end

"""
struct MapIterator{C,I}
    call::C # callable
    iter::I # iterable
end

MapIterator(call, n::Integer) =
    MapIterator(call, Base.OneTo(n))

_iterate(imap::MapIterator, next::Nothing) = nothing

_iterate(imap::MapIterator, next::Tuple{Any,Any}) =
    (imap.call(next[1]), next[2])

Base.iterate(imap::MapIterator) =
    _iterate(imap, iterate(imap.iter))

Base.iterate(imap::MapIterator, state) =
    _iterate(imap, iterate(imap.iter, state))

end # module
