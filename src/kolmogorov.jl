#
# kolmogorov.jl --
#
# Generation of a Kolmogorov phase screen.
#
#-------------------------------------------------------------------------------
#
# This file is part of the TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2019, Éric Thiébaut.
#

using Random

"""

```julia
kolmogorov([T=Float64,] dims, D_over_r0=1; all=false, orig=false, seed=-1)
```

yields a 2-dimensional array of dimensions `dims` filled with random phases
which follow Kolmogorov law for a ratio pupil diamleter bu Fried's parameter
equal to `D_over_r0`.  If a Point Spread Function is to be calculated from the
generated phase screen, it should be appropriately sampled (i.e., `D_over_r0`
less than `N/2` with `N` the number of samples across the pupil diameter).
Optional argument `T` is to specify the floating-point type of the computed
phases.

The algorithm is the mid-point method of R.G. Lane, A. Glindemann and
J.C. Dainty (``Simulation of a Kolmogorov phase screen'' in Waves in Random
Media, 2, 209-224, 1992).  Boolean keyword `orig` indicates whether or not the
original method by Lane et al. should be used (default is to not use the
original algorithm but a slightly modified version).

Boolean keyword `all` indicates whether or not all the computed phase screen
should be returned.  The default behaviour is to return the smallest array into
which a pupil with a diameter of `diam` samples can fit.  The computed phase
screen is a `(2^n+1)×(2^n+1)` array with `n` integer.

Keyword `seed` can be set with a nonnegative integer to reinitialized the
random sequence (see [`Random.seed!`](@ref)).

The in-place version:

```julia
kolmogorov!(A, D_over_r0=1; orig=false, seed=-1)
```

overwrites the 2-dimensional array `A` with a generated phase screen. The
dimensions of `A` must be equal to `2^N +1` for some integer `N`.  The in-place
version does not consume extra memory.

"""
kolmogorov(siz1::Integer, siz2::Integer, D_over_r0::Real = 1.0; kwds...) =
    kolmogorov((siz1, siz2), D_over_r0; kwds...)

kolmogorov(siz::NTuple{2,Integer}, D_over_r0::Real = 1.0; kwds...) =
    kolmogorov(Float64, siz, D_over_r0; kwds...)

function kolmogorov(::Type{T}, siz1::Integer, siz2::Integer,
                    D_over_r0::Real = one(T); kwds...) where {T}
    kolmogorov(T, (siz1, siz2), D_over_r0; kwds...)
end

function kolmogorov(::Type{T}, siz::NTuple{2,Integer},
                    D_over_r0::Real = one(T); kwds...) where {T}
    kolmogorov(T, (Int(siz[1]), Int(siz[2])), D_over_r0; kwds...)
end

function kolmogorov(::Type{T}, siz::NTuple{2,Int},
                    D_over_r0::Real = one(T);
                    all::Bool=false, kwds...) where {T}
    @assert T <: AbstractFloat
    siz1, siz2 = siz
    @assert siz1 ≥ 3
    @assert siz2 ≥ 3

    # Compute the size of the minimum array of size 2^N + 1 which holds the
    # phase screen.
    mindim = max(siz1, siz2)
    nbits = 0
    dim = 1
    while dim < mindim
        nbits += 1
        dim = (1 << nbits) + 1
    end

    # Generate phase screen.
    A = kolmogorov!(Array{T,2}(undef, dim, dim), T(D_over_r0); kwds...)
    if all
        return A
    end

    # Extract the central part of the screen.
    i1 = (dim - siz1) >> 1
    i2 = (dim - siz2) >> 1
    return A[1+i1:siz′+i1, 1+i2:siz2+i2]
end

function kolmogorov!(A::AbstractMatrix{T},
                     D_over_r0::Real=one(T);
                     orig::Bool=false,
                     seed::Integer=-1) where {T<:AbstractFloat}
    # Check dimensions.
    @assert !Base.has_offset_axes(A)
    siz1, siz2 = size(A)
    siz1 == siz2 || error("array must be square")
    n = siz1
    nbits = 0
    while (1 << nbits) + 1 < n
        nbits += 1
    end
    if n != (1 << nbits) + 1
        error("array dimensions must be equal to 2^N + 1 for some integer N")
    end

    if seed ≥ 0
        # Seed the random generator and reset related storage.
        Random.seed!(seed)
    end

    # Parameters.
    local delta::T, beta::T, b1::T, b2::T
    local c1::T, c2::T, c3::T, c4::T, c5::T, l5::T, m5::T
    delta = sqrt(6.88*D_over_r0^(5/3))
    beta = 1.7817974
    c1 = 3.3030483e-1*delta
    c2 = 6.2521894e-1*delta
    c3 = 5.3008502e-1*delta
    c4 = 3.9711507e-1*delta
    if orig
        c5 = 4.5420202e-1*delta
    else
        c5 = 4.4355177e-1*delta
        l5 = 4.5081546e-1
        m5 = 9.8369088e-2
    end
    b1 = c2*randn(T)
    b2 = c2*randn(T)

    # 4 first corners
    A[1,1] = c1*randn(T) + b1
    A[n,1] = c1*randn(T) + b2
    A[1,n] = c1*randn(T) - b2
    A[n,n] = c1*randn(T) - b1

    # all other points
    h = n - 1
    @inbounds while h ≥ 2
        s = h   # step size
        h >>= 1 # half the step size
        c3 /= beta
        c4 /= beta
        c5 /= beta

        # centre of squares
        for k in 1+h:s:n-h        # mid-point coordinates
            kp, kn = k-h, k+h     # previous and next points
            for j in 1+h:s:n-h    # mid-point coordinates
                jp, jn = j-h, j+h # previous and next points
                #println("$jp, $jn, $kp, $kn")
                A[j,k] = c3*randn(T) + 0.25*(A[jp,kp] + A[jp,kn] +
                                             A[jn,kp] + A[jn,kn])
            end
        end

        if 2s < n
            # centre of losanges
            for k in 1+s:s:n-s        # vertice coordinates
                kp, kn = k-h, k+h     # previous and next points
                for j in 1+h:s:n-h    # mid-point coordinates
                    jp, jn = j-h, j+h # previous and next points
                    A[j,k] = c4*randn(T) + 0.25*(A[j,kp] + A[j,kn] +
                                                 A[jp,k] + A[jn,k])
                    A[k,j] = c4*randn(T) + 0.25*(A[k,jp] + A[k,jn] +
                                                 A[kp,j] + A[kn,j])
                end
            end
        end

        # borders
        if orig
            for j in 1+h:s:n-h    # mid-point coordinates
                jp, jn = j-h, j+h # previous and next points
                A[1,j] = c5*randn(T) + 0.5*(A[1,jp] + A[1,jn])
                A[n,j] = c5*randn(T) + 0.5*(A[n,jp] + A[n,jn])
                A[j,1] = c5*randn(T) + 0.5*(A[jp,1] + A[jn,1])
                A[j,n] = c5*randn(T) + 0.5*(A[jp,n] + A[jn,n])
            end
        else
            for j in 1+h:s:n-h    # mid-point coordinates
                jp, jn = j-h, j+h # previous and next points
                A[1,j] = c5*randn(T) + l5*(A[1,jp] + A[1,jn]) + m5*A[1+h,j]
                A[n,j] = c5*randn(T) + l5*(A[n,jp] + A[n,jn]) + m5*A[n-h,j]
                A[j,1] = c5*randn(T) + l5*(A[jp,1] + A[jn,1]) + m5*A[j,1+h]
                A[j,n] = c5*randn(T) + l5*(A[jp,n] + A[jn,n]) + m5*A[j,n-h]
            end
        end
    end
    return A
end

@doc @doc(kolmogorov) kolmogorov!
