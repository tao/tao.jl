#
# types.jl --
#
# Type definitions in TAO, a Toolkit for Adaptive Optics.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2021, Éric Thiébaut.
#

"""
    TimeoutError()

yields an exception indicating that a timeout occured.

"""
struct TimeoutError <: Exception; end

"""
    Tao.AbstractTimeValue

is the abstract time for types implementing the time value interface of TAO.

Concrete instances of `Tao.AbstractTimeValue` shall be structures with two integer fields
representing the integer and fractional parts of the time value respectively in
seconds and in `1/n`-th of seconds with `n` the resolution of the time value.
A constructor:
T(ip,fp)

Concrete instance `t` of `Tao.AbstractTimeValue` shall implement the following
methods:

    Tao.build(T,ip,fp) # yields a time value of type `T` with integer part `ip` and fractional part `fp`
    Tao.resolution(T)  # yields the resolution `n` of time value type `T`
    Tao.time_secs(t)   # yields the integer part (in seconds) of time value `t`
    Tao.time_frac(t)   # yields the fractional part (in `1/n`-th of seconds) of time value `t`

Note that when `time_secs` and `time_frac` are applied to a time value type,
they shall return the type of the corresponding field.

"""
abstract type AbstractTimeValue end

"""
    Tao.TimeValue

is the union of time value types expressing time in seconds with integer and
fractional parts stored as integers.

"""
const TimeValue = Union{TimeVal,AbstractTimeValue}

"""
    Tao.Forever

is the singleton type used to indicate an unlimited time limit.

"""
struct Forever end

"""
    Tao.TimeLimit

is the union of possible types to specify a time limit. Such instances are time
values, reals assumed to be in seconds, and [`Tao.forever`](@ref).

"""
const TimeLimit = Union{TimeValue,Real,Forever}

"""
    WeightedArray(wgt, dat)

yields and instance of `WeightedArray` which stores an array of data `dat` with
corresponding weights `wgt`.  The conventions are that all weights and data
have finite values (to avoid numerical issues in computations) and that weights
are nonnegative (a weight of zero indicates an invalid data value).

"""
struct WeightedArray{T<:AbstractFloat,N,
                     W<:DenseArray{T,N},
                     D<:DenseArray{T,N}}
    wgt::W
    dat::D
    # This unique inner constructor is to make sure that the two arrays have
    # the same indices.  There is however no way to prevent resizing the arrays
    # if they are resizable (e.g. ordinary vectors).
    function WeightedArray{T,N,W,D}(wgt::W, dat::D) where {T<:AbstractFloat,N,
                                                           W<:DenseArray{T,N},
                                                           D<:DenseArray{T,N}}
        axes(wgt) == axes(dat) || throw(DimensionMismatch(
            "weights and data must have the same indices"))
        return new{T,N,W,D}(wgt, dat)
    end
end

const WeightedVector{T,W,D} = WeightedArray{T,1,W,D}
const WeightedMatrix{T,W,D} = WeightedArray{T,2,W,D}

"""
    AbstractCamera{T,N}

is the super-type of cameras and image providers in TAO. Parameter `T` is the
pixel type of the produced images or `Any` when the pixel type is unknown or
undetermined. Parameter `N` is the number of dimensions of the produced images,
usually 2 for ordinary images and 3 for weighted images. Base methods `eltype`
and `ndims` can be used to retrieve parameters `T` and `N`.

"""
abstract type AbstractCamera{T,N} end

"""
    CameraOutput

is the super-type of concrete types used to specify the ouptput of a camera or
an image provider.

"""
abstract type CameraOutput end

"""
    SingleImage

is a singleton sub-type of [`Tao.CameraOutput`](@ref) and is used to require a
single image from a camera or an image provider.

"""
struct SingleImage <: CameraOutput end

"""
    WeightedImage

is a singleton sub-type of [`Tao.CameraOutput`](@ref) and is used to require a
pre-processed image with its weights from a camera or an image provider.

"""
struct WeightedImage <: CameraOutput end
