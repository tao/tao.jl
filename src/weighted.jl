#
# weighted.jl --
#
# Implementation of weighted arrays.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2020, Éric Thiébaut.
#

function WeightedArray(wgt::W, dat::D,
                       verify::Bool=false) where {T<:AbstractFloat,N,
                                                  W<:DenseArray{T,N},
                                                  D<:DenseArray{T,N}}
    if verify
        @inbounds for i in eachindex(wgt)
            w = wgt[i]
            if ! isfinite(w) || w < 0
                throw(ArgumentError("weights must be finite and nonnegative"))
            end
        end
        @inbounds for i in eachindex(dat)
            if ! isfinite(dat[i])
                throw(ArgumentError("data must be finite"))
            end
        end
    end
    return WeightedArray{T,N,W,D}(wgt, dat)
end

Base.ndims(A::WeightedArray{T,N}) where {T,N} = N
Base.eltype(A::WeightedArray{T,N}) where {T,N} = T
Base.length(A::WeightedArray) = length(values(A))
Base.size(A::WeightedArray) = size(values(A))
Base.size(A::WeightedArray, d) = size(values(A), d)
Base.axes(A::WeightedArray) = axes(values(A))
Base.axes(A::WeightedArray, d) = axes(values(A), d)

"""
    weights(A)

yields the array of weights of the weighted array `A`.

Also see [`WeightedArray`](@ref), [`values`](@ref).

"""
weights(A::WeightedArray) = A.wgt

"""
    values(A)

yields the array of values of the weighted array `A`.

Also see [`WeightedArray`](@ref), [`weights`](@ref).

"""
values(A::WeightedArray) = A.dat

"""
    fix_weigthed_data(wgt, dat) -> fixwgt, fixdat

yields weights `fixwgt` and data `fixdat` whose values have been computed from
the weights `wgt` and the data `dat` so that for each index `i`:

  *`fixwgt[i] = wgt[i]` and `fixdat[i] = dat[i]` if `dat[i]` is finite and
    `wgt[i]` is finite and nonnegative;

  * `fixwgt[i] = 0` and `fixdat[i] = 0` otherwise.


The operation can be done in-place:

    fix_weigthed_data!(wgt, dat) -> wgt, dat

or by providing the resulting arrays (which can be the same as the input
arrays):

    fix_weigthed_data!(fixwgt, fixdat, wgt, dat) -> fixwgt, fixdat

The pair of arrays `(wgt,dat)` and `(fixwgt,fixdat)` can be repaced by an
instance of [`WeightedArray`](@ref).

"""
function fix_weigthed_data(wgt::AbstractArray{T,N},
                           dat::AbstractArray{T,N}) where {T<:AbstractFloat,N}
    return fix_weigthed_data!(similar(wgt), similar(dat), wgt, dat)
end

fix_weigthed_data(A::WeightedArray) =
    WeightedArray(fix_weigthed_data(weights(A), values(A))...)

function fix_weigthed_data!(wgt::AbstractArray{T,N},
                            dat::AbstractArray{T,N}) where {T<:AbstractFloat,N}
    return fix_weigthed_data!(wgt, dat, wgt, dat)
end

function fix_weigthed_data!(fixwgt::AbstractArray{T,N},
                            fixdat::AbstractArray{T,N},
                            wgt::AbstractArray{T,N},
                            dat::AbstractArray{T,N}) where {T<:AbstractFloat,N}
    axes(fixwgt) == axes(fixdat) == axes(wgt) == axes(dat) ||
        throw(DimensionMismatch("all arrays must have the same indices"))
    @inbounds for i in eachindex(fixwgt, fixdat, wgt, dat)
        w, d = wgt[i], dat[i]
        if isfinite(d) && isfinite(w) && w ≥ 0
            fixwgt[i] = w
            fixdat[i] = d
        else
            fixwgt[i] = 0
            fixdat[i] = 0
        end
    end
    return (fixwgt, fixdat)
end

function fix_weigthed_data!(A::WeightedArray)
    fix_weigthed_data!(weights(A), values(A))
    return A
end

function fix_weigthed_data!(dst::WeightedArray, A::WeightedArray)
    fix_weigthed_data!(weights(dst), values(dst), weights(A), values(A))
    return dst
end

@doc @doc(fix_weigthed_data) fix_weigthed_data!
