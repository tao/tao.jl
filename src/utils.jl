#
# utils.jl --
#
# Various general purpose functions.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2024, Éric Thiébaut.
#

"""
    Tao.exported_symbols(mod)

yields a curated list of the symbols exported by module `mod`.

"""
exported_symbols(mod::Module) = begin
    symbols = Symbol[]
    for sym in names(mod, all=false, imported=false)
        str = String(sym)
        if !(startswith(str, '.') || startswith(str, '_'))
            push!(symbols, sym)
        end
    end
    return symbols
end

"""
    Tao.warning([io=stderr,] ...)

prints a warning message in color.

"""
warning(args...) = warning(stderr, args...)

function warning(io::IO, args...)
    printstyled(io, "WARNING: "; bold=true, color=:yellow)
    printstyled(io, args..., "\n"; bold=false, color=:yellow)
end

"""
    Tao.floating_point_type(obj) -> T<:AbstractFloat

yields the floating-point type used for computations by object `obj`.  This is
not the same as `eltype(obj)` which yields the type of the elements of the
results provided by `obj`.

"""
floating_point_type(::T) where {T<:AbstractFloat} = T
floating_point_type(::Type{T}) where {T<:AbstractFloat} = T
floating_point_type(::AbstractArray{T}) where {T<:AbstractFloat} = T
floating_point_type(::Type{<:AbstractArray{T}}) where {T<:AbstractFloat} = T

"""
    Tao.to_float(args...)

if arguments `args...` are values, promote them to a common floating-point
type; otherwise, if arguments are types, return their promoted floating-point
type.  The promoted floating-point type is `Float64` if all arguments are
integer values or types.

"""
to_float(x::AbstractFloat) = x
to_float(x::Integer) = Float64(x)

to_float(vals::Real...) = to_float(promote(vals...)...)
to_float(vals::T...) where {T<:AbstractFloat} = vals
to_float(vals::T...) where {T<:Integer} = map(Float64, vals)

to_float(types::Type{<:Real}...) = float(promote_type(types...))
to_float(T::Type{<:AbstractFloat}) = T
to_float(::Type{<:Integer}) = Float64

"""
    of_eltype(T, A)

yields an object similar to `A` but with elements of type `T`.  If the elements
of `A` are already of the requested type, `A` is returned, not a copy.

"""
of_eltype(::Type{T}, A::AbstractArray{T}) where {T} = A
of_eltype(::Type{T}, A::AbstractArray) where {T} = copyto!(similar(A, T), A)

#------------------------------------------------------------------------------
# DATE

import Dates

"""
    file_prefix(dt = Dates.now(Dates.UTC))

yields the prefix used for TAO data files and based on the UTC date `dt`.
The result is a string of the form: `"YYmmdd/YYYYmmdd-HHMMSS-"`
or just `"YYYYmmdd-HHMMSS-"` if keyword `basename` is true.

"""
file_prefix(dt::Dates.DateTime = Dates.now(Dates.UTC); basename::Bool=false) =
    Dates.format(dt, (basename ? "yyyymmdd-HHMMSS-" :
                      "yymmdd/yyyymmdd-HHMMSS-"))

#------------------------------------------------------------------------------
# LAYOUT

"""
    Tao.indexed_layout(inds) -> cnt, xinds
    Tao.indexed_layout(msk, orient=0) -> cnt, xinds

yield a 2D array of indices related to the layout of some device with a finite
number `cnt` of inputs or outputs (e.g. the sub-pupils of a wave-front sensor
or the actuators of a deformable mirror).  The result `xinds` is a fresh array
of integers set to zero for unused positions and otherwise set with an index in
the range `1:cnt` corresponding to a specific input or output of the device.

- `inds` is a 2D array of indices whose elements are set following the same
  rules as for `xinds`.  In that case, the reason to call `indexed_layout` is
  to get a new array (independent from `inds`) with checked contents.

- `msk` is a 2D array of booleans whose elements are true (resp. false) at used
  (resp. unused) locations and `orient` is an integer which specifies how the
  used should be numbered. The 1st bit of `orient` specifies whether the
  numbering decreases along the first axis of `msk`. The 2nd bit of `orient`
  specifies whether the numbering decreases along the second axis of `msk`. The
  3rd bit of `orient` specifies whether to swap the axes in the result.

"""
function indexed_layout(msk::AbstractMatrix{Bool},
                        orient::Integer = 0)
    Base.has_offset_axes(msk) && error("mask array has non-standard indexing")

    # What is the direction of the numbering?
    reverse_1 = (orient & 1) != 0
    reverse_2 = (orient & 2) != 0
    swap_axes = (orient & 4) != 0

    # Fill array of indices according to direction of numbering.
    n1, n2 = size(msk)
    inds = Array{Int,2}(undef, (swap_axes ? (n2, n1) : (n1, n2)))
    cnt = 0
    for i2 in 1:n2
        j2 = (reverse_2 ? n2+1-i2 : i2)
        for i1 in 1:n1
            j1 = (reverse_1 ? n1+1-i1 : i1)
            if msk[i1,i2]
                cnt += 1
                if swap_axes
                    inds[j2,j1] = cnt
                else
                    inds[j1,j2] = cnt
                end
            else
                if swap_axes
                    inds[j2,j1] = 0
                else
                    inds[j1,j2] = 0
                end
            end
        end
    end
    return cnt, inds
end

# Make a private copy with checked contents.
function indexed_layout(inds::AbstractArray{<:Integer})
    Base.has_offset_axes(inds) &&
        error("array of indices has non-standard indexing")
    xinds = zeros(Int, size(inds))

    # First pass to count the number of valid indices and check that they are
    # all specified exactly once.  The destination array is used as a workspace
    # to keep track of how many times a valid index is specified.
    len = length(inds)
    cnt = 0
    jmax = 0
    @inbounds for i in eachindex(inds)
        j = Int(inds[i])
        if j > 0
            j ≤ len || error("out of range index")
            xinds[j] == 0 || error("multiple occurences of the same index")
            xinds[j] = 1
            jmax = max(jmax, j)
            cnt += 1
        elseif j < 0
            error("indices must be all nonnegative")
        end
    end
    jmax == cnt || error("some indices in index range are missing")

    # Second pass: just copy the indices.
    @inbounds for i in eachindex(xinds, inds)
        xinds[i] = inds[i]
    end
    return cnt, xinds
end

#------------------------------------------------------------------------------
# PUPIL SHAPE

"""
    pupil_shape(args...) -> msk

yields a 2D mask (a Julia matrix of boolean elements) defining the valid/active
positions in a pupil.

"""
pupil_shape(rows::AbstractString...) = pupil_shape(rows)
function pupil_shape(rows::Union{AbstractVector{<:AbstractString},
                                 Tuple{Vararg{AbstractString}}})
    nrows = length(rows)
    ncols = length(rows[1])
    A = Array{Bool,2}(undef, ncols, nrows)
    for y in 1:nrows
        row = rows[y]
        length(row) == ncols || error("all rows must have the same length")
        for x in 1:ncols
            A[x,y] = !isspace(row[x])
        end
    end
    return A
end

pupil_shape(A::AbstractMatrix{Bool}) = A

#------------------------------------------------------------------------------
# SELECTION OF ENTRIES

"""
    Tao.select(A, sel)

yields the values of array `A` for which the predicate `sel(A[i])` is true.
Argument `sel` may also be a boolean array indicating which elements of `A` to
select.

This method is equivalent to `filter(sel,A)` except for the order of the
arguments and for the possibility to use a boolean mask.

"""
function select(A::AbstractArray{T,N},
                sel::AbstractArray{Bool,N}) where {T,N}
    axes(A) == axes(sel) || dimension_mismatch(
        "arguments `A` and `sel` must have the same indices")
    # FIXME: count(sel) is as fast as the following code
    n = 0
    @inbounds for i in eachindex(sel)
        n += (sel[i] ? 1 : 0)
    end
    return select!(Array{T}(undef, n), A, sel)
end

function select(A::AbstractArray, sel)
    n = 0
    @inbounds for i in eachindex(A)
        n += (sel(A[i]) ? 1 : 0)
    end
    return select!(Array{eltype(A)}(undef, n), A, sel)
end

"""
    Tao.select!(dst, A, sel) -> dst

overwrites the contents of `dst` with the values of array `A` for which the
predicate `sel(A[i])` is true.  Argument `sel` may also be a boolean array
indicating which elements of `A` to select.  It is asserted that the length of
the destination `dst` is exactly the number of selected elements.

"""
function select!(dst::AbstractVector{<:Any},
                 A::AbstractArray{<:Any,N},
                 sel::AbstractArray{Bool,N}) where {N}
    axes(A) == axes(sel) || dimension_mismatch(
        "arguments `A` and `sel` must have the same indices")
    J = Base.axes1(dst)
    j = first(J) - 1
    n = last(J)
    @inbounds for i in eachindex(A, sel)
        if sel[i]
            j += 1
            j ≤ n || error("too many selected values")
            dst[j] = A[i]
        end
    end
    j == n || error("too few selected values")
    return dst
end

function select!(dst::AbstractVector, A::AbstractArray, sel)
    J = Base.axes1(dst)
    j = first(J) - 1
    n = last(J)
    @inbounds for i in eachindex(A)
        if sel(A[i])
            j += 1
            j ≤ n || error("too many selected values")
            dst[j] = A[i]
        end
    end
    j == n || error("too few selected values")
    return dst
end

#------------------------------------------------------------------------------
# VECTORIZED OPERATIONS

function add!(Opt::Type{<:OptimLevel},
              dst::AbstractArray{<:Any,N},
              src::AbstractArray{<:Any,N}) where {N}
    @assert axes(dst) == axes(src)
    @maybe_vectorized Opt for i in eachindex(dst, src)
        dst[i] += src[i]
    end
    return dst
end

function scale!(Opt::Type{<:OptimLevel},
                A::AbstractArray{T},
                alpha::Real) where {T<:AbstractFloat}
    return scale!(Opt, A, T(alpha))
end

function scale!(Opt::Type{<:OptimLevel},
                A::AbstractArray{T},
                alpha::T) where {T<:AbstractFloat}
    if alpha == 0
        fill!(A, zero(T))
    elseif alpha != 1
        @maybe_vectorized Opt for i in eachindex(A)
            A[i] *= alpha
        end
    end
    return A
end

# The following is a replacement of lmul! that may be faster than the BLAS
# version for small arrays, smaller than say 100×100.  FIXME: should be part of
# MayOptimize.
function LinearAlgebra.lmul!(opt::Type{<:OptimLevel},
                             A::LowerTriangular{T},
                             b::AbstractVector{T}) where {T<:AbstractFloat}
    I, J = axes(A)
    @assert I == J
    @assert Base.axes1(b) == J
    @maybe_inbounds opt for i in reverse(I)
        s = zero(T)
        @maybe_vectorized P for j in J
            s += A[i,j]*b[j]
        end
        b[i] = s
    end
    return b
end

# Manage to call Julia version (probably BLAS).
function LinearAlgebra.lmul!(::Type{<:MayOptimize.Standard},
                             A::LowerTriangular{T},
                             b::AbstractVector{T}) where {T<:AbstractFloat}
    lmul!(A, b)
    return b
end

#------------------------------------------------------------------------------
# ERRORS

"""
    Tao.dimension_mismatch(args...)

throws a `DimensionMismatch` exception with `args...` converted into a string.
If no arguments are specified, the message is:

    "arguments have incompatible dimensions"

"""
@noinline dimension_mismatch(mesg::String) = throw(DimensionMismatch(mesg))
@noinline dimension_mismatch(args...) = dimension_mismatch(string(arsg...))
@noinline dimension_mismatch() =
    dimension_mismatch("arguments have incompatible dimensions")

"""
    Tao.argument_error(args...)

throws an `ArgumentError` exception with `args...` converted into a string.

"""
@noinline argument_error(mesg::String) = throw(ArgumentError(mesg))
@noinline argument_error(args...) = argument_error(string(arsg...))
