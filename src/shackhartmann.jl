#
# shackhartmann.jl --
#
# Implement Shack-Hartmann wavefront sensors in TAO.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2024, Éric Thiébaut.
#

"""
    ShackHartmannGeometry(dims, subs, inds) -> geom

build an object storing the geometry of a Shack-Hartmann wavefront sensor with
`dims` the size of the images acquirred by the wavefront sensor camera, `subs`
the bounding-boxes or the ROIs of the sub-images, and `inds` a matrix of
sub-image indices defining the layout of the sub-images in a rectangular grid
of sub-pupils: `inds[i,j] = k` with `k ≥ 1` if the `k`-th sub-image sits in the
cell `(i,j)` and `k = 0` otherwise.

The following methods are available:

    length(geom)                       # number of sub-images
    Tao.image_size(geom)               # size of the wavefront sensor image
    Tao.subimages_bounding_boxes(geom) # bounding-boxes of the sub-images
    Tao.layout(geom)                   # indexed layout of the sub-images

"""
struct ShackHartmannGeometry
    dims::Dims{2}  # wavefront sensor image dimensions
    boxes::Vector{BoundingBox{Int}} # bounding boxes of the sub-images
    inds::Matrix{Int} # indexed layout
    function ShackHartmannGeometry(dims::NTuple{2,Integer},
                                   boxes::AbstractVector{<:BoundingBox{<:Integer}},
                                   inds::AbstractMatrix{<:Integer})
        # Minimal check, not as completely as `indexed_layout` but enough to
        # make sure that indices are not out of bounds.
        dims[1] ≥ 1 && dims[2] ≥ 1 || error("invalid image dimensions")
        cnt = 0
        imin = typemax(Int)
        imax = typemin(Int)
        for i in inds
            if i > 0
                cnt += 1
                imin = min(imin, Int(i))
                imax = max(imax, Int(i))
            end
        end
        (imin == 1 && imax == cnt) || error("invalid layout indice(s)")
        length(boxes) == cnt || error("invalid number of bounding-boxes")
        imagebox = BoundingBox{Int}(1:dims[1], 1:dims[2])
        for box in boxes
            if isempty(box) || !(box ⊆ imagebox)
                error("invalid sub-image bounding-box:", box)
            end
        end
        new(dims, boxes, inds)
    end
end

Base.length(geom::ShackHartmannGeometry) = length(subimages_bounding_boxes(geom))
subimages_bounding_boxes(geom::ShackHartmannGeometry) = geom.boxes
layout(geom::ShackHartmannGeometry) = geom.inds
image_size(geom::ShackHartmannGeometry) = geom.dims

function ShackHartmannGeometry(dims::NTuple{2,Integer},
                               boxes::AbstractVector{<:BoundingBox{<:Integer}},
                               mask::AbstractMatrix{Bool},
                               orient::Integer = 0)
    cnt, inds = indexed_layout(inds)
    return ShackHartmannGeometry(dims, boxes, inds)
end

# FIXME: doc!
mutable struct ShackHartmannSensor{T,
                                   S<:AbstractWavefrontMeasure{T},
                                   A<:AbstractWavefrontSensorAlgorithm{S},
                                   C<:AbstractCamera{T}} <: AbstractWavefrontSensor{S}
    geom::ShackHartmannGeometry
    cam::C # image provider connected to the wavefront sensor camera
    alg::A # wavefront sensor algorithm
    ref::Vector{Point{T}} # WFS reference positions

    # To avoid (re)allocations, the wavefront sensor measurements are stored in
    # an array that is owned by the wavefront sensor object (much like the
    # image provider which caches the pre-processed image and its weights).
    meas::Vector{S} # cached measurements
    counter::Int # loop counter

    function ShackHartmannSensor{T,S,A,C}(geom::ShackHartmannGeometry,
                                          cam::C,
                                          alg::A,
                                          ref::AbstractVector{<:Point{<:Real}},
                                          meas::AbstractVector{S}) where {
                                              T,
                                              S<:AbstractWavefrontMeasure{T},
                                              A<:AbstractWavefrontSensorAlgorithm{S},
                                              C<:AbstractCamera{T}
                                          }
        # Minimal check to make sure that indices are not out of bounds.
        len = length(geom)
        length(alg)  == len || error("invalid number of sub-images in algorithm")
        length(ref)  == len || error("invalid number of reference positions")
        length(meas) == len || error("invalid number of measurements")
        new{T,S,A,C}(geom, cam, alg, ref, meas, 0)
    end
end

# Constructors.
function ShackHartmannSensor(
    geom::ShackHartmannGeometry,
    cam::C,
    alg::A,
    ref::AbstractVector{<:Point{<:Real}} = default_reference(Point{T}, geom)) where {
        T<:AbstractFloat,
        S<:AbstractWavefrontMeasure{T},
        A<:AbstractWavefrontSensorAlgorithm{S},
        C<:AbstractCamera{T}}
    ShackHartmannSensor{T,S,A,C}(geom, cam, alg, ref,
                                 Vector{S}(undef, length(ref)))
end

# Implement API of a wavefront-sensor.
Base.length(wfs::ShackHartmannSensor) = length(geometry(wfs))
Base.size(wfs::ShackHartmannSensor) = size(layout(wfs))
algorithm(wfs::ShackHartmannSensor) = wfs.alg
camera(wfs::ShackHartmannSensor) = wfs.cam
geometry(wfs::ShackHartmannSensor) = wfs.geom
device(wfs::ShackHartmannSensor) = device(camera(wfs))
image_size(wfs::ShackHartmannSensor) = image_size(geometry(wfs))
layout(wfs::ShackHartmannSensor) = layout(geometry(wfs))
measurements(wfs::ShackHartmannSensor) = wfs.meas
reference(wfs::ShackHartmannSensor) = wfs.ref
set_reference!(wfs::ShackHartmannSensor, ref::AbstractVector{<:Point}) = begin
    @assert length(ref) == length(wfs)
    copyto!(reference(wfs), ref)
    set_reference!(algorithm(wfs), reference(wfs))
    nothing
end
subimages_bounding_boxes(wfs::ShackHartmannSensor) =
    subimages_bounding_boxes(geometry(wfs))

function default_reference(T::Type{<:Point{<:AbstractFloat}},
                           geom::ShackHartmannGeometry)
    default_reference(T, Tao.subimages_bounding_boxes(geom))
end

function default_reference(T::Type{<:Point{<:AbstractFloat}},
                           boxes::AbstractVector{<:BoundingBox{<:Integer}})
    ref = Vector{T}(undef, length(boxes))
    @inbounds for i in eachindex(ref, boxes)
        ref[i] = center(boxes[i])
    end
    return ref
end
