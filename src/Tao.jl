# Tao.jl -
#
# Julia package TAO, a Toolkit for Adaptive Optics.
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao) licensed under
# the MIT license.
#
# Copyright (C) 2018-2024, Éric Thiébaut.

module Tao

export
    # Cameras.
    AbstractCamera,
    CameraOutput,
    SingleImage,
    WeightedImage,

    # Deformable mirrors.
    AbstractDeformableMirror,

    # Wavefront sensors.
    AbstractWavefrontMeasure,
    AbstractWavefrontSensor,
    AbstractWavefrontSensorAlgorithm,
    AbstractWavefrontSlope,
    ShackHartmannGeometry,
    ShackHartmannSensor,
    WavefrontSlope,
    WeightedWavefrontSlope,

    # From TwoDimensional.
    AbstractPoint, Point, BoundingBox,

    # Methods for remote objects.
    abort, configure, kill, reset, send, start, stop,

    # Other methods.
    calibrate, fit, camera, algorithm, layout,

    # Utilities.
    floating_point_type,

    # Times.
    AbstractTimeValue,
    Forever,
    TimeVal, # from Base.Libc
    TimeValue,
    TimeLimit,

    # Miscellaneous.
    TimeoutError,
    readfits,
    writefits,
    writefits!,

    # Statistics.
    mean,
    std,
    var

using TwoDimensional
import TwoDimensional: AbstractPoint, Point, BoundingBox

using EasyFITS
import EasyFITS: write!, hduname, readfits, writefits, writefits!

using MayOptimize
using LinearAlgebra
using Requires

import Base: read, write, reset, kill
import Sockets: send

import Statistics: mean, std, var

import Base.Libc: TimeVal

# Undocumented methods that can be imported by sub-packages, notably TaoRT.
function abort end
function algorithm end
function calibrate end
function camera end
function configure end
function fit end
function layout end
function start end
function stop end
function wait_output end
function wait_command end

# FIXME: Remove the following.
function device end
function reference end
function set_reference! end
function image_size end
function measurements end
function send! end
function subimages_bounding_boxes end

"""
    Tao.measure!(dest, alg, [wgt,] img) -> dest

computes the wavefront measurements into `dest` with algorithm `alg` and given
the wavefront sensor image `img` and, optionally, its respective weights `wgt`.

In a control loop, this method is called immediately after each acquired image
and it shall deliver the measurements as quickly as possible.  If additional
processing is required, they can be performed by the method `Tao.postprocess`
which is called after sending the commands and before waiting for the next
image.  The method `Tao.reset` is called on the algorithm `alg` on entry of the
loop.

A control loop looks like:

    reset(alg)
    while true
         wgt, img = timedwait(WeightedImage, cam, timeout)
         measure!(meas, alg, wgt, img)
         ... # <--- compute command and send it to the deformable mirror
         postprocess(alg)
     end

with `cam` is the wavefront sensor camera and `meas` is the storage for the
measurements.

Using the higher level interface, this simplifies to:

    reset(wfs)
    while true
         meas = timedwait(wfs, timeout)
         ... # compute command and send it to the deformable mirror
     end

with `wfs` the wavefront sensor which encapsulate and algoritm, a camera and
storage for the measurments.

See also: [`Tao.reset`](@ref) and [`Tao.postprocess`](@ref),

"""
function measure! end

"""
    Tao.postprocess(alg)

applies any postprocessing required by algorithm `alg`.  In a control loop,
this method is called after sending the commands and before waiting for the
next image.

See also: [`Tao.initialize`] and [`Tao.measure!`],

"""
function postprocess end

include("types.jl")
include("times.jl")
include("utils.jl")
include("weighted.jl")

include("geometry.jl")
for sym in exported_symbols(Geometry)
    @eval import .Geometry: $sym
end

include("iterators.jl")
for sym in exported_symbols(Iterators)
    @eval import .Iterators: $sym
end

include("cameras.jl")

include("wavefrontsensors.jl")
for sym in exported_symbols(WavefrontSensors)
    @eval import .WavefrontSensors: $sym
end

include("deformablemirrors.jl")
for sym in exported_symbols(DeformableMirrors)
    @eval import .DeformableMirrors: $sym
end

function __init__()
    @require TaoRT="e97054c3-40bb-4251-82f9-3abb7bf225d1" begin
        import .TaoRT: RemoteCamera, RemoteMirror, SharedArray
    end
end

end # module
