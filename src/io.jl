#
# io.jl --
#
# Input/output methods to load/save data in TAO.
#
#------------------------------------------------------------------------------
#
# This file is part of the TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2020, Éric Thiébaut.
#

# FIXME: write!(FitsFile, "20191115-interaction-matrix.fits", (FitsHeader(HDUNAME="INTERACTION-CALIBRATION-U",HDUVERS=1,FPS=(1200.0,"Frame per seconds")),U1), (FitsHeader(HDUNAME="INTERACTION-CALIBRATION-V",HDUVERS=1,FPS=(1200.0,"Frame per seconds")),V1))


# Convert an array of points into a regular array.
# FIXME: Also provide the reciprocal.
# FIXME: Perhaps use reinterpret((T,T), A)...
# FIXME: This is kind of "type piracy", should be done by `TwoDimensional`
#        package.
Base.Array(A::AbstractArray{<:AbstractPoint{T}}) where {T} = Array{T}(A)
function Base.Array{T}(A::AbstractArray{Point{T}}) where {T<:AbstractFloat}
    arr = Array{T}(undef, 2, size(A)...)
    @inbounds for i in CartesianIndices(A)
        arr[1,i] = A[i].x
        arr[2,i] = A[i].y
    end
    return arr
end

# Convert an array of wavefront slopes into a regular array.
Base.Array(A::AbstractArray{WeightedWavefrontSlope{T}}) where {T} = Array{T}(A)
function Base.Array{T}(A::AbstractArray{<:WeightedWavefrontSlope}
                       ) where {T<:AbstractFloat}
    # FIXME: using reinterpret((T,T), A) would be great!
    arr = Array{T}(undef, 5, size(A)...)
    @inbounds for i in CartesianIndices(A)
        arr[1,i] = A[i].x
        arr[2,i] = A[i].y
        arr[3,i] = A[i].wxx
        arr[4,i] = A[i].wxy
        arr[5,i] = A[i].wyy
    end
    return arr
end

EasyFITS.write(io::FitsFile, A::AbstractArray{<:AbstractPoint}; kwds...) =
    write(io, Array(A); kwds...)

function EasyFITS.write(io::FitsFile,
                        A::AbstractArray{WeightedWavefrontSlope{T}};
                        kwds...) where {T}
    write(io, Array(A); kwds...)
end

# FIXME: Create Tao.Telemetry type and then write(Tao.Telemetry, path, ...)
function save_telemetry(path::AbstractString,
                        cmd::AbstractArray{<:AbstractFloat,2},
                        dat::AbstractArray{<:WeightedWavefrontSlope,2};
                        kwds...)
    save_telemetry(path, dat, cmd; kwds...)
end

function save_telemetry(path::AbstractString,
                        dat::AbstractArray{<:WeightedWavefrontSlope,2},
                        cmd::AbstractArray{<:AbstractFloat,2};
                        overwrite::Bool=false, kwds...)
    @assert size(dat, 2) == size(cmd, 2)
    (overwrite == false && exists(path)) &&
        throw_file_already_exists(path, "try with `overwrite=true`")
    FitsFile(path, (overwrite ? "w!" : "w")) do io
        write(io, dat; HDUNAME="WEIGHTED-WAVEFRONT-SLOPE", kwds...)
        write(io, cmd; HDUNAME="DEFORMABLE-MIRROR-COMMANDS")
    end
    nothing
end
