#
# wavefrontsensors.jl --
#
# API for wavefront sensors in TAO.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2023, Éric Thiébaut.
#

module WavefrontSensors

export
    AbstractWavefrontMeasure,
    AbstractWavefrontSensor,
    AbstractWavefrontSensorAlgorithm,
    AbstractWavefrontSlope,
    ShackHartmannGeometry,
    ShackHartmannSensor,
    WavefrontSlope,
    WeightedWavefrontSlope,
    abort,
    algorithm,
    camera,
    device,
    image_size,
    layout,
    measure!,
    postprocess,
    measurements,
    reference,
    reset,
    set_reference!,
    start,
    stop,
    subimages_bounding_boxes

using ArrayTools
using TwoDimensional
using ..Tao
using ..Tao: AbstractCamera, indexed_layout, to_float
using ..Tao: TimeLimit
import ..Tao:
    abort,
    algorithm,
    camera,
    device,
    floating_point_type,
    image_size,
    layout,
    measure!,
    measurements,
    postprocess,
    reference,
    reset,
    set_reference!,
    start,
    stop,
    subimages_bounding_boxes

import Base: Array, convert, copyto!

using EasyFITS
using EasyFITS: throw_file_already_exists
import EasyFITS: read, write, write!, hduname, readfits, writefits, writefits!

"""
    AbstractWavefrontMeasure{T}

is the super-type of the various concrete measure types provided by wavefront
sensors.  Parameter `T` is the floating-point type of the measurements.

[`Tao.WavefrontSlope`](@ref) and [`Tao.WavefrontSlope`](@ref) are concrete
types of wavefront sensor measurements.

Method [`Tao.floating_point_type`](@ref) yields `T`.

"""
abstract type AbstractWavefrontMeasure{T<:AbstractFloat} end

"""
    AbstractWavefrontSensorAlgorithm{S}

is the super-type of the various concrete algorithms used by wavefront sensors
to extract their measurements.  Parameter `S` is the type of the measurements
provided by the algorithm.

Basic method `eltype` applied to a wavefront sensor algorithm yields the type
of measurements given be the algorithm.

"""
abstract type AbstractWavefrontSensorAlgorithm{S<:AbstractWavefrontMeasure} end

"""
    AbstractWavefrontSensor{S}

is the super-type of the various concrete wavefront sensor types.  Parameter
`S` is the type of the measurements provided by the sensor.

Basic method `eltype` applied to a wavefront sensor yields the type of
measurements given be the wavefront sensor.

"""
abstract type AbstractWavefrontSensor{S<:AbstractWavefrontMeasure} end

"""
    AbstractWavefrontSlope{T}

is the super-type of the various concrete wavefront sensor slope types.
Parameter `T` is the floating-point type of the slopes.

Instances of descendant types have fields `x` and `y`.  Method
[`Tao.floating_point_type`](@ref) yields `T`.

[`Tao.WavefrontSlope`](@ref) and [`Tao.WavefrontSlope`](@ref) are concrete
types descendant of`Tao.AbstractWavefrontSlope{T}`.

"""
abstract type AbstractWavefrontSlope{T} <: AbstractWavefrontMeasure{T} end

# FIXME: doc!
struct WavefrontSlope{T} <: AbstractWavefrontSlope{T}
    x::T
    y::T
end

# FIXME: doc!
struct WeightedWavefrontSlope{T} <: AbstractWavefrontSlope{T}
    x::T
    y::T
    wxx::T
    wxy::T
    wyy::T
end

WavefrontSlope(S::WavefrontSlope) = S
WavefrontSlope(x::Real, y::Real) = WavefrontSlope(to_float(x, y)...)
WavefrontSlope(x::T, y::T) where {T<:AbstractFloat} = WavefrontSlope{T}(x, y)
WavefrontSlope(xy::NTuple{2,Real}) = WavefrontSlope(xy...)
WavefrontSlope(P::Point) = WavefrontSlope(P.x, P.y)
WavefrontSlope(S::AbstractWavefrontSlope) = WavefrontSlope(S.x, S.y)

WavefrontSlope{T}(S::WavefrontSlope{T}) where {T} = S
WavefrontSlope{T}(xy::NTuple{2,Real}) where {T<:AbstractFloat} =
    WavefrontSlope{T}(xy...)
WavefrontSlope{T}(P::Point) where {T<:AbstractFloat} =
    WavefrontSlope{T}(P.x, P.y)
WavefrontSlope{T}(S::AbstractWavefrontSlope) where {T<:AbstractFloat} =
    WavefrontSlope{T}(S.x, S.y)

WeightedWavefrontSlope(S::WeightedWavefrontSlope) = S
WeightedWavefrontSlope(x::Real, y::Real, wxx::Real, wxy::Real, wyy::Real) =
    WeightedWavefrontSlope(to_float(x, y, wxx, wxy, wyy)...)
WeightedWavefrontSlope(x::T, y::T, wxx::T, wxy::T, wyy::T) where {T<:AbstractFloat} =
    WeightedWavefrontSlope{T}(x, y, wxx, wxy, wyy)
WeightedWavefrontSlope(tup::NTuple{5,Real}) = WeightedWavefrontSlope(tup...)
WeightedWavefrontSlope(xy::NTuple{2,Real}, wgt::NTuple{3,Real}) =
    WeightedWavefrontSlope(xy..., wgt...)

WeightedWavefrontSlope{T}(S::WeightedWavefrontSlope{T}) where {T} = S
WeightedWavefrontSlope{T}(S::WeightedWavefrontSlope) where {T<:AbstractFloat} =
    WeightedWavefrontSlope{T}(S.x, S.y, S.wxx, S.wxy, S.wyy)
WeightedWavefrontSlope{T}(tup::NTuple{5,Real}) where {T<:AbstractFloat} =
    WeightedWavefrontSlope{T}(tup...)
WeightedWavefrontSlope{T}(xy::NTuple{2,Real}, wgt::NTuple{3,Real}) where {T<:AbstractFloat} =
    WeightedWavefrontSlope{T}(xy..., wgt...)

TwoDimensional.Point(S::AbstractWavefrontSlope) = Point(S.x, S.y)
TwoDimensional.Point{T}(S::AbstractWavefrontSlope) where {T} =
    Point{T}(S.x, S.y)

Base.Tuple(S::WavefrontSlope) = (S.x, S.y)
Base.Tuple(S::WeightedWavefrontSlope) = (S.x, S.y, S.wxx, S.wxy, S.wyy)

convert(::Type{T}, S::AbstractWavefrontSlope) where {T<:Point} = T(S)
convert(::Type{T}, P::AbstractPoint) where {T<:WavefrontSlope} = T(P.x, P.y)

convert(::Type{T}, xy::NTuple{2,Real}) where {T<:WavefrontSlope} = T(xy)
convert(::Type{T}, tup::NTuple{5,Real}) where {T<:WeightedWavefrontSlope} =
    T(tup)

convert(::Type{T}, S::T) where {T<:AbstractWavefrontSlope} = S
convert(::Type{T}, S::AbstractWavefrontSlope) where {T<:WavefrontSlope} = T(S)
convert(::Type{T}, S::WeightedWavefrontSlope) where {T<:WeightedWavefrontSlope} = T(S)

# Make possible something like: (x,y,wxx,wxy,wyy) = s with s an instance of
# WeightedWavefrontSlope.
function Base.iterate(itr::AbstractWavefrontSlope,
                      state::Tuple{Tuple,Int} = (Tuple(itr), 1))
    vals, i = state
    return (i ≤ length(vals) ? (vals[i], (vals, i+1)) : nothing)
end

floating_point_type(::Type{<:AbstractWavefrontMeasure{T}}) where {T} = T
floating_point_type(x::AbstractWavefrontMeasure) =
    floating_point_type(typeof(x))

for X in (:AbstractWavefrontSensor,
          :AbstractWavefrontSensorAlgorithm)
    @eval begin
        floating_point_type(::T) where {T<:$X} = floating_point_type(T)
        floating_point_type(::Type{<:$X{S}}) where {S} = floating_point_type(S)
        Base.eltype(::$X{S}) where {S} = S
        Base.eltype(::Type{<:$X{T}}) where {T} = T
    end
end

"""
    Tao.algorithm(wfs) -> alg

yields the algorithm used by the wavefront sensor `wfs` to compute the
measurements.

"""
function algorithm end

"""
    Tao.camera(wfs) -> cam

yields the camera used by the wavefront sensor `wfs`.

"""
function camera end

"""
    Tao.device(wfs) -> dev

yields the camera device used by the wavefront sensor `wfs`.

"""
function device end

"""
    Tao.reference(wfs) -> ref

yields the set of reference positions of the wavefront sensor `wfs`.

"""
function reference end

"""
    Tao.set_reference!(wfs, ref) -> reference(wfs)

overwrites the set of reference positions of the wavefront sensor `wfs` with
the set of positions `ref` and return the resulting reference positions.

"""
set_reference!(wfs::AbstractWavefrontSensor, ref::AbstractString) =
    set_reference!(wfs, readfits(Array, ref)) # FIXME: be more specific

set_reference!(wfs::AbstractWavefrontSensor, ref::AbstractMatrix{<:Real}) = begin
    n = length(wfs)
    axes(ref) == (1:2, 1:n) || throw(DimensionMismatch("invalid reference dimensions"))
    T = float(eltype(ref))
    xref = Vector{Point{T}}(undef, n)
    for i in 1:n
        xref[i] = Point(ref[1,i], ref[2,i])
    end
    set_reference!(wfs, xref)
end

"""
    Tao.measurements(wfs) -> data

yields the last measurements computed by the wavefront sensor `wfs`.  The
result is an array of the measurements whose types are given by `eltype(wfs)`.

!!! Note

    For efficiency reasons, the last measurements are stored by the wavefront
    sensor and should be copied if the caller wants to modify them.

"""
function measurements end

# Camera API.
"""
    Tao.start(wfs, args...; kwds...)

starts acquisition by the camera associated with wavefront sensor `wfs`.

"""
start(wfs::AbstractWavefrontSensor, args...; kwds...) =
    start(camera(wfs), args...; kwds...)

"""
    Tao.stop(wfs, args...; kwds...)

stops acquisition by the camera associated with wavefront sensor `wfs`.

"""
stop(wfs::AbstractWavefrontSensor) = stop(camera(wfs))

"""
    Tao.abort(wfs, args...; kwds...)

aborts acquisition by the camera associated with wavefront sensor `wfs`.

"""
abort(wfs::AbstractWavefrontSensor) = abort(camera(wfs))

"""
    Tao.reset(wfs) -> wfs

resets the measurement algorithm used by the wavefront sensor `wfs`.

"""
reset(wfs::AbstractWavefrontSensor) = begin
    wfs.counter = 0
    reset(algorithm(wfs))
end

include("shackhartmann.jl")

# This private method is needed to avoid ambiguities in timedwait.
function _timedwait(wfs::AbstractWavefrontSensor,
                    timeout::Union{TimeLimit,Real})
    # If we are between 2 iterations of the loop, apply the algorithm
    # post-processing.
    if wfs.counter > 0
        postprocess(algorithm(wfs))
    end
    # Wait for next processed image and return the measured slopes.
    wgt, img = timedwait(WeightedImage, camera(wfs), timeout)
    wfs.counter += 1
    return measure!(measurements(wfs), algorithm(wfs), wgt, img)
end
Base.timedwait(wfs::AbstractWavefrontSensor, timeout::TimeLimit) =
    _timedwait(wfs, timeout)
Base.timedwait(wfs::AbstractWavefrontSensor, timeout::Real) =
    _timedwait(wfs, timeout)

# FIXME: flatten -> reinterpret

"""
    Tao.flatten(A) -> B

yields a floating point array `B` storing the slopes in array `A`.  The result
`B` has one more dimension than `A`, the first dimension of `B` is 2 or 5
depending on the type of wavefront slopes and the trailing dimensions of `B`
are the same as the dimensions of `A`.  For any Cartesian index `I` indexing
array `A`, the contents of `B` is given by:

    B[1,I] = A[I].x
    B[2,I] = A[I].x
    B[3,I] = A[I].wxx # only for weighted wavefront slopes
    B[4,I] = A[I].wxy # idem
    B[5,I] = A[I].wyy # idem

The same result can be obtained by calling `Tao.copy_contents!(B,A)` with `B`
of suitable size and type.

"""
flatten(A::AbstractArray{<:WavefrontSlope{T},N}) where {T,N} =
    copy_contents!(Array{T,N+1}(undef, 2, standard_size(A)...), A)

flatten(A::AbstractArray{<:WeightedWavefrontSlope{T},N}) where {T,N} =
    copy_contents!(Array{T,N+1}(undef, 5, standard_size(A)...), A)


#
# Copy wavefront slopes into an array of floating-points (and conversely).
#
# We cannot just extend `copyto!` because it has a different semantic.
#

function copy_contents!(dst::AbstractArray{<:AbstractFloat},
                        src::AbstractArray{<:WavefrontSlope})
    I = axes(src)
    axes(dst) == (Base.OneTo(2), I...) || throw_incompatible_dimensions()
    @inbounds @simd for i in CartesianIndices(I)
        dst[1,i] = src[i].x
        dst[2,i] = src[i].y
    end
    return dst
end

function copy_contents!(dst::AbstractArray{<:WavefrontSlope},
                        src::AbstractArray{<:AbstractFloat})
    I = axes(dst)
    axes(src) == (Base.OneTo(2), I...) || throw_incompatible_dimensions()
    @inbounds @simd for i in CartesianIndices(I)
        dst[i] = (src[1,i], src[2,i])
    end
    return dst
end

function copy_contents!(dst::AbstractArray{<:AbstractFloat},
                        src::AbstractArray{<:WeightedWavefrontSlope})
    I = axes(src)
    axes(dst) == (Base.OneTo(5), I...) || throw_incompatible_dimensions()
    @inbounds @simd for i in CartesianIndices(I)
        dst[1,i] = src[i].x
        dst[2,i] = src[i].y
        dst[3,i] = src[i].wxx
        dst[4,i] = src[i].wxy
        dst[5,i] = src[i].wyy
    end
    return dst
end

function copy_contents!(dst::AbstractArray{<:WeightedWavefrontSlope},
                        src::AbstractArray{<:AbstractFloat})
    I = axes(dst)
    axes(src) == (Base.OneTo(5), I...) || throw_incompatible_dimensions()
    @inbounds @simd for i in CartesianIndices(I)
        dst[i] = (src[1,i], src[2,i], src[3,i], src[4,i], src[5,i])
    end
    return dst
end

#
# Converting wavefront slopes into an array of floating-points (and conversely).
#
# We just extend Array{...} constructor which is what is called by the convert
# method.
#
# Something like `Array{<:WavefrontSlope}(A)` yields the expected result.
#

Array{T}(A::AbstractArray{<:AbstractWavefrontSlope,N}) where {T<:AbstractFloat,N} =
    Array{T,N+1}(A)
Array{T,N}(A::AbstractArray{<:AbstractWavefrontSlope}) where {T<:AbstractFloat,N} =
    convert(Array{T,N}, A)

for (W,M) in ((:WavefrontSlope, 2),
              (:WeightedWavefrontSlope, 5))
    @eval begin

        Array{<:$W}(A::AbstractArray{T,N}) where {T<:AbstractFloat,N} =
            Array{$W{T},N-1}(A)

        Array{<:$W,N}(A::AbstractArray{T}) where {T<:AbstractFloat,N} =
            Array{$W{T},N}(A)

        function Array{S}(A::AbstractArray{<:AbstractFloat,N}) where {T<:AbstractFloat,
                                                                      N,S<:$W{T}}
            Array{S,N-1}(A)
        end

        function Array{T,N}(A::AbstractArray{<:$W}) where {T<:AbstractFloat,N}
            N == ndims(A) + 1 || error("incompatible number of dimensions")
            Base.has_offset_axes(A) && error("array must have standard indexing")
            copy_contents!(Array{T,N}(undef, $M, size(A)...), A)
        end

        function Array{$W{T},N}(A::AbstractArray{<:AbstractFloat}) where {T<:AbstractFloat,N}
            ndims(A) == N+1 || error("incompatible number of dimensions")
            Base.has_offset_axes(A) && error("array must have standard indexing")
            dims = size(A)
            dims[1] == $M || error("invalid array size (1st dimension must be $M)")
            copy_contents!(Array{$W{T}}(undef, dims[2:N+1]), A)
        end

    end
end

#------------------------------------------------------------------------------
# FITS FILES

const WAVEFRONT_SLOPES = "TAO WAVEFRONT SLOPES";
const WEIGHTED_WAVEFRONT_SLOPES = "TAO WEIGHTED WAVEFRONT SLOPES";

const WavefrontSensorData = AbstractArray{<:AbstractWavefrontMeasure}
const TypeOrInstance{X} = Union{X,Type{<:X}}

EasyFITS.hduname(::TypeOrInstance{AbstractArray{<:WavefrontSlope}}) =
    (WAVEFRONT_SLOPES, 1)

EasyFITS.hduname(::TypeOrInstance{AbstractArray{<:WeightedWavefrontSlope}}) =
    (WEIGHTED_WAVEFRONT_SLOPES, 1)

description(::TypeOrInstance{AbstractArray{<:WavefrontSlope}}) =
    "wavefront slopes"
description(::TypeOrInstance{AbstractArray{<:WeightedWavefrontSlope}}) =
    "weighted wavefront slopes"

comment(::TypeOrInstance{AbstractArray{<:WavefrontSlope}}) =
    "Wavefront sensor measurements"
comment(::TypeOrInstance{AbstractArray{<:WeightedWavefrontSlope}}) =
    "Weighted wavefront sensor measurements"

naxis1(::TypeOrInstance{AbstractArray{<:WavefrontSlope}}) = 2
naxis1(::TypeOrInstance{AbstractArray{<:WeightedWavefrontSlope}}) = 5

# Use FITS format by default.
write(filename::AbstractString, data::WavefrontSensorData; kwds...) =
    writefits(filename, data; kwds...)
write(filename::AbstractString, hdr, data::WavefrontSensorData; overwrite::Bool = false) =
    writefits(filename, hdr, data; overwrite = overwrite)
write!(filename::AbstractString, data::WavefrontSensorData; kwds...) =
    writefits!(filename, data; kwds...)
write!(filename::AbstractString, hdr, data::WavefrontSensorData) =
    writefits!(filename, hdr, data)

# Silently overwrite destination file.
writefits!(filename::AbstractString, data::WavefrontSensorData; kwds...) =
    write(filename, data; overwrite = true, kwds...)
writefits!(filename::AbstractString, hdr, data::WavefrontSensorData) =
    write!(filename, hdr, data; overwrite = true)

# First build the FitsHeader instance to avoid creating an empty file when
# building the header throws an exception.
function writefits(filename::AbstractString,
                   data::WavefrontSensorData; overwrite::Bool = false, kwds...)
    writefits(filename, FitsHeader(; kwds...), data; overwrite = overwrite)
end
function writefits(filename::AbstractString, hdr,
                   data::WavefrontSensorData; overwrite::Bool = false)
    write(filename, FitsHeader(hdr), data; overwrite = overwrite)
end
write(file::FitsFile, data::WavefrontSensorData; kwds...) =
    write(file, FitsHeader(; kwds...), data)

# Create a new FITS file.
function writefits(filename::AbstractString, hdr::Union{FitsHeader,Nothing},
                   data::WavefrontSensorData; overwrite::Bool = false)
    (overwrite == false && ispath(filename)) && throw_file_already_exists(
        path, "call `write!` or `fitswrite!`, or use `overwrite=true`")
    FitsFile(filename, (overwrite ? "w!" : "w")) do file
        write(file, hdr, data)
    end
end

# Append new HDU to a FITS file.
function write(file::FitsFile, hdr, data::WavefrontSensorData)
    name, vers = hduname(data)
    A = flatten(data)
    hdu = FitsImageHDU(file, floating_point_type(A), size(A))
    hdu["HDUNAME"] = (name, comment(data))
    hdu["HDUVERS"] = (vers, "Revision number of this extension")
    merge!(hdu, hdr)
    write(hdu, A)
    nothing
end

# Read wavefront sensor data from a FITS file.
read(T::Type{<:WavefrontSensorData}, filename::AbstractString; kwds...) =
    readfits(T, filename; kwds...)
readfits(T::Type{<:WavefrontSensorData}, filename::AbstractString; kwds...) =
    FitsFile(filename, "r") do file
        ead(T, file; kwds...)
    end
function read(T::Type{<:WavefrontSensorData}, file::FitsFile; ext::Integer = 1)
    name, _ = hduname(T)
    k = findnext(hdu -> hdu.hduname == name, file, ext)
    k === nothing && error(
        "no HDU with $(description(T)) found at or after HDU number $ext")
    read(T, file[k])
end

# Read wavefront sensor data from a FITS HDU.
function read(T::Type{<:WavefrontSensorData}, hdu::FitsImageHDU)
    name, _ = hduname(T)
    hdu.hduname == name || error("bad HDUNAME, should be \"$name\"")
    dims = hdu.data_size
    length(dims) ≥ 1 && dims[1] == naxis1(T) || error(
        "invalid data size (NAXIS1 must be $(naxis1(T)))")
    hdu.data_eltype <: AbstractFloat || error(
        "invalid data type (expecting floating-point)")
    return convert(T, read(hdu))
end

@noinline throw_incompatible_dimensions() =
    throw(DimensionMismatch("incompatible dimensions"))

end # module
