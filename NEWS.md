# User visible changes in Tao

## Version 0.10.1

- New `AbstractTimeValue` type and API to deal with time values.
  This is a generalization of the API formerly in `TaoRT`.
