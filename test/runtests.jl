module TestingTao

using Test, Tao
import Dates

@testset "Utilities" begin
    mask = Bool[0 1 1 1 0
                1 1 1 1 1
                1 1 1 1 1
                1 1 1 1 1
                0 1 1 1 0]
    for orient in 0:7
        cnt, inds = Tao.indexed_layout(mask, orient);
        @test cnt == count(mask)
        @test extrema(inds) == (0, cnt)
        _, xinds = Tao.indexed_layout(inds);
        @test xinds == inds
    end
    dt = Dates.now(Dates.UTC)
    prefix = Tao.file_prefix(dt)
    @test basename(prefix) == Tao.file_prefix(dt, basename=true)
    @test dirname(prefix) == SubString(prefix, 1, 6)
    @test dirname(prefix) == SubString(prefix, 10, 15)
end

end # module
