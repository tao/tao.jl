# Julia framework for adaptive optics systems

This package provides the Julia framework for TAO a *Toolkit for Adaptive
Optics* systems.  This framework defines common (abstract) types and methods
used by the sub-packages of TAO.


## Installation

First add [`EmmtRegistry`](https://github.com/emmt/EmmtRegistry) to your
registries:

```julia
julia> using Pkg
julia> pkg"registry add https://github.com/emmt/EmmtRegistry"
```

Then install `Tao`, just type the following command from Julia package
manager:

```julia
julia> pkg"add Tao"
```
